from setuptools import setup, find_packages
setup(
    entry_points={'console_scripts': ['vicharm=projectspec:main']},
    name="Vicharm",
    package_dir={'': 'src'},
    packages=find_packages(),
    py_modules=["projectspec"],
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    version="0.1",
)
