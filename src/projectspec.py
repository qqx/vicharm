import argparse
import json
import logging
import os
import sys

log = logging.getLogger(__name__)
handler = logging.StreamHandler(stream=sys.stderr)
log.addHandler(handler)
log.setLevel(logging.DEBUG)


CONFIG_PATH = os.path.join(os.environ['HOME'], "Projects", ".projects.json")
log.debug("CONFIG_PATH: %s", CONFIG_PATH)


def venv_command(shell, venv):
    if shell == "fish":
        return "vf activate {}".format(venv)
    else:
        raise NotImplementedError


def vim_command(shell, find):
    if shell == "fish":
        return "nvim ( {} )".format(find)
    else:
        raise NotImplementedError


def read_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "name", help="The name of the project (a key in the .project.yml file).")
    parser.add_argument(
        "-n", "--novim", help="Open vim or not", action="store_true")
    args = parser.parse_args()
    return args


def main():
    args = read_args()
    log.debug("args: %s", args)
    project_name = args.name
    novim = args.novim

    with open(CONFIG_PATH, "r") as fh:
        configs = json.load(fh)
    log.debug("All configs: %s", configs)
    project_config = configs[project_name]
    log.debug("%s config: %s", project_name, project_config)

    location = project_config['location']
    find = project_config['find']
    venv = project_config['venv']

    with sys.stdout as fh:
        fh.write("cd {}\n".format(location))
        fh.write("".join([venv_command("fish", venv), "\n"]))
        if not novim:
            fh.write("".join([vim_command("fish", find), "\n"]))

if __name__ == "__main__":
    main()
