Vicharm
-------

A Python developer productivity tool. For a project, moves you to the project directory, activates the venv, and opens the project files in nvim.


How it works
------------
Create a json file at the root of your projects directory. This file lists out the project name, its location in the file system, the virtual env it uses, and the find command to pipe tovim.

A script reads this file and produces a shell script approprate for sourcing. The shell script will change directory to the project root, activate the virtual env, and optionally open vim with all the project files.


Limitations
-----------
This is for now a personal project, working with the fish shell.


Road map
--------

* Extend to use other shells
* Use yaml instead of json (easier maintainability)
* Add tests
* Add shell functions so that the source step is not necessary.
* Optionally set window name for use with GNU screen.


Installation
------------

.. code::

    python setup.py install


Configuration
-------------

.. code::

    touch ~/Projects/.projects.json
    echo {"<projectname>": {"location": "/path/to/project", "name": "My Project Name", "venv": "myprojectvenvname", "find":"<fully escaped find command"}} >> ~/Projects/.projects.json


Usage
-----

.. code::

    # for fish
    vicharm <projectname> [-n|--novim] | source -


    # or via the call_vicharm function
    source <vicharm-repo>/shells/test.fish
    call_vicharm <projectname> [-n|--novim]
